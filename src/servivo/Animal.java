/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package servivo;

/**
 *
 * @author User
 */
    
public class Animal extends SerVivo{
    
    private boolean colunaVertebral;
    private boolean racional;

    public boolean isColunaVertebral() {
        return colunaVertebral;
    }

    public void setColunaVertebral(boolean colunaVertebral) {
        this.colunaVertebral = colunaVertebral;
    }

    public boolean isRacional() {
        return racional;
    }

    public void setRacional(boolean racional) {
        this.racional = racional;
    }

}

